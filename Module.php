<?php

/*
Tämä on uusin versio ZEND:in AuthenticationService integrointi Symfony:n doctrineen.
*/

namespace AuthDoctrine;

use Zend\ServiceManager\ServiceManager;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;


class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getViewHelperConfig()
    {
        return [
            'aliases' => [
                'apphelper' => AuthHelper::class,
            ],
            'factories' => [
                AuthHelper::class => View\AuthHelperController::class,
            ],
        ];
    }
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
	
    public function getServiceConfig()
    {
        return array(
        'factories' => array(
			'Zend\Authentication\AuthenticationService' => function($serviceManager) {
                return $serviceManager->get('doctrine.authenticationservice.orm_default');
            },
            'mail.transport' => function (ServiceManager $serviceManager) {
                $config = $serviceManager->get('Config'); 
                $transport = new Smtp();                
                $transport->setOptions(new SmtpOptions($config['mail']['transport']['options']));
                return $transport;
            },
            ),
        );
    }
}	